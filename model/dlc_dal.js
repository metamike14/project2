var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM game_dlc_view;'

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};


exports.getById = function(dlc_id, callback) {
    var query = 'SELECT * FROM DLC WHERE dlc_id = (?);';
    var queryData = [dlc_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO DLC (vg_id, dlc_name, date_released, rating) VALUES (?)';

    var queryData = [params.vg_id, params.dlc_name, params.date_released, params.rating];

    connection.query(query, [queryData], function(err, result) {
        callback(err, result.insertId);
    });

};

exports.delete = function(dlc_id, callback) {
    var query = 'DELETE FROM DLC WHERE dlc_id = ?';
    var queryData = [dlc_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE DLC SET dlc_name = ?, date_released = ?, rating = ? WHERE dlc_id = ?;';
    var queryData = [params.dlc_name, params.date_released, params.rating, params.dlc_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(dlc_id, callback) {
    var query = 'SELECT * FROM DLC WHERE dlc_id = (?);';
    var queryData = [dlc_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};