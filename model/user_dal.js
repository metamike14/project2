var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM user;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(user_id, callback) {
    var query = 'SELECT * FROM user WHERE user_id = (?)';
    var queryData = [user_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE user
    var query = 'INSERT INTO user (first_name, last_name, email) VALUES (?)';

    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query,[queryData], function(err, result) {
        var user_id = result.insertId;
        var query = 'INSERT INTO library (user_id, vg_id) VALUES ?';
        var userLibraryData = [];
        if (params.vg_id.constructor === Array) {
            for (var i = 0; i < params.vg_id.length; i++) {
                userLibraryData.push([user_id, params.vg_id[i]]);
            }
        }
        else {
            userLibraryData.push([user_id, params.vg_id]);
        }
        connection.query(query, [userLibraryData], function(err, result){
            callback(err, result);
        });
    });

};

exports.delete = function(user_id, callback) {
    var query = 'DELETE FROM user WHERE user_id = ?';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE user SET email = ?, first_name = ?, last_name = ? WHERE user_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.user_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'DELETE FROM library WHERE user_id = ?';
        connection.query(query, params.user_id, function(err, result) {
            var query = 'INSERT INTO library (user_id, vg_id) VALUES ?';
            var userLibraryData = [];
            if (params.vg_id.constructor === Array) {
                for (var i = 0; i < params.vg_id.length; i++) {
                    userLibraryData.push([params.user_id, params.vg_id[i]]);
                }
            }
            else {
                userLibraryData.push([params.user_id, params.vg_id]);
            }
            connection.query(query, [userLibraryData], function(err, result){
                callback(err, result);
            });
        });
    });
};

exports.edit = function(user_id, callback) {
    var query = 'SELECT * FROM user WHERE user_id = (?);';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.library = function(user_id, callback) {
    var query = 'CALL get_user_library(?);';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.videogameInfo = function(user_id, vg_id, callback) {
    var query = 'CALL get_user_videogame(?,?);';
    var queryData = [user_id, vg_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.updateVG = function(params, callback) {
    var query;
    var queryData;
    if(params.last_played == ""){
        query = 'UPDATE library SET date_purchased = ?, play_time = ? WHERE user_id = ? AND vg_id = ?';
        queryData = [params.date_purchased, params.play_time, params.user_id, params.vg_id];
    }else{
        query = 'UPDATE library SET date_purchased = ?, last_played = ?, play_time = ?, user_rating = ? WHERE user_id = ? AND vg_id = ?';
        queryData = [params.date_purchased, params.last_played, params.play_time, params.user_rating, params.user_id, params.vg_id];
    }
    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};