var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM developer;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(developer_id, callback) {
    var query = 'SELECT * FROM developer WHERE dev_id = (?)';
    var queryData = [developer_id];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE developer
    var query = 'INSERT INTO developer (dev_name, type, status, city, state_country) VALUES (?)';

    var queryData = [params.dev_name, params.type, params.status, params.city, params.state_country];

    connection.query(query,[queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(developer_id, callback) {
    var query = 'DELETE FROM developer WHERE dev_id = ?';
    var queryData = [developer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE developer SET dev_name = ?, type = ?, status = ?, city = ?, state_country = ? WHERE dev_id = ?';
    var queryData = [params.dev_name, params.type, params.status, params.city, params.state_country, params.dev_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(developer_id, callback) {
    var query = 'SELECT * FROM developer WHERE dev_id = (?);';
    var queryData = [developer_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.search = function(dev_avg, callback) {
    var query = 'SELECT avg(v.rating) as avg, d.* FROM videogame v\n' +
        'LEFT JOIN game_dev gd on gd.vg_id = v.vg_id\n' +
        'LEFT JOIN developer d on d.dev_id = gd.dev_id\n' +
        'GROUP BY d.dev_id\n' +
        'HAVING avg > (?);';
    var queryData = [dev_avg];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};