var express = require('express');
var router = express.Router();

// View the dev for the given id
router.get('/', function(req, res){
    res.render('about/aboutMain');
});

router.get('/erd', function(req, res){
    res.render('about/aboutERD');
});

router.get('/relateSchema', function(req, res) {
    res.render('about/aboutSchema');
});

module.exports = router;
