var express = require('express');
var router = express.Router();
var publisher_dal = require('../model/publisher_dal');


// View All pub
router.get('/all', function(req, res) {
    publisher_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('publisher/publisherViewAll', { pub: result });
        }
    });

});

// View the pub for the given id
router.get('/', function(req, res){
    if(req.query.pub_id == null) {
        res.send('pub_id is null');
    }
    else {
        publisher_dal.getById(req.query.pub_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('publisher/publisherViewById', {pub: result[0]});
           }
        });
    }
});

router.get('/add', function(req, res){
    res.render('publisher/publisherAdd');
});

// View the pub for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.pub_name == null) {
        res.send('pub_name is required.');
    }
    else if(req.query.city == null) {
        res.send('Publisher must have a city');
    }
    else if(req.query.state_country == null) {
        res.send('Publisher must have a state/country');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        publisher_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the pub to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/publisher/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.pub_id == null) {
        res.send('A pub id is required');
    }
    publisher_dal.edit(req.query.pub_id, function(err, result){
        res.render('publisher/publisherUpdate', { pub: result[0]});
    });
});

router.get('/update', function(req, res) {
    publisher_dal.update(req.query, function(err, result){
       res.redirect(302, '/publisher/all');
    });
});

// Delete a pub for the given pub_id
router.get('/delete', function(req, res){
    if(req.query.pub_id == null) {
        res.send('pub_id is null');
    }
    else {
         publisher_dal.delete(req.query.pub_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/publisher/all');
             }
         });
    }
});

module.exports = router;
