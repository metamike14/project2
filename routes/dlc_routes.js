var express = require('express');
var router = express.Router();
var dlc_dal = require('../model/dlc_dal');
var videogame_dal = require('../model/videogame_dal');



// View All dlcs
router.get('/all', function(req, res) {
    dlc_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('dlc/dlcViewAll', { dlc: result });
        }
    });

});

// View the dlc for the given id
router.get('/', function(req, res){
    if(req.query.dlc_id == null) {
        res.send('dlc_id is null');
    }
    else {
        dlc_dal.getById(req.query.dlc_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('dlc/dlcViewById', {dlc: result});
           }
        });
    }
});

// Return the add a new dlc form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    res.render('dlc/dlcAdd', {vg: req.query.vg_id});
});

router.get('/add/selectvg', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    videogame_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('dlc/dlcSelectVG', { vg: result});
        }
    });
});

// View the dlc for the given id
router.post('/insert', function(req, res){
    // simple validation
    if(req.body.dlc_name == null) {
        res.send('DLC name must be provided.');
    }
    else if(req.body.date_released == null) {
        res.send('Needs a release date');
    }
    else if(req.body.rating == null) {
        res.send('Need a score');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        dlc_dal.insert(req.body, function(err,dlc_id) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                dlc_dal.edit(dlc_id, function (err, result) {
                    res.render('dlc/dlcUpdate', {
                        dlc: result[0],
                        was_successful: true
                    });
                });
            }
        });
    }
});


router.get('/edit', function(req, res){
    if(req.query.dlc_id == null) {
        res.send('A dlc id is required');
    }
    dlc_dal.edit(req.query.dlc_id, function (err, result) {
        res.render('dlc/dlcUpdate', {dlc: result[0]});
    });
});

router.get('/update', function(req, res) {
    dlc_dal.update(req.query, function(err, result){
       res.redirect(302, '/dlc/all');
    });
});

// Delete a dlc for the given dlc_id
router.get('/delete', function(req, res){
    if(req.query.dlc_id == null) {
        res.send('dlc_id is null');
    }
    else {
         dlc_dal.delete(req.query.dlc_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/dlc/all');
             }
         });
    }
});

module.exports = router;
