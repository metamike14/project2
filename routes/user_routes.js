var express = require('express');
var router = express.Router();
var user_dal = require('../model/user_dal');
var videogame_dal = require('../model/videogame_dal');

// View All user
router.get('/all', function(req, res) {
    user_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('user/userViewAll', { user: result });
        }
    });

});

// View the user for the given id
router.get('/', function(req, res){
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    else {
        user_dal.getById(req.query.user_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('user/userViewById', {user: result[0]});
           }
        });
    }
});

router.get('/add', function(req, res){
    videogame_dal.getAll(function(err, vg){
        res.render('user/userAdd',  {vg: vg });
    });
});

// View the user for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == null) {
        res.send('Email is required.');
    }
    else if(req.query.first_name == null) {
        res.send('user must have a first name');
    }
    else if(req.query.last_name == null) {
        res.send('user must have a last name');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        user_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/user/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.user_id == null) {
        res.send('A user id is required');
    }
    videogame_dal.getAll(function(err, full_vg){
        user_dal.library(req.query.user_id, function(err, vg){
            user_dal.edit(req.query.user_id, function(err, result){
                res.render('user/userUpdate', { user: result[0], full_vg: full_vg, vg: vg[0]});
            });
        });
    });
});

router.get('/update', function(req, res) {
    user_dal.update(req.query, function(err, result){
       res.redirect(302, '/user/all');
    });
});

// Delete a user for the given user_id
router.get('/delete', function(req, res){
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    else {
         user_dal.delete(req.query.user_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/user/all');
             }
         });
    }
});

router.get('/library', function (req, res) {
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    else {
        user_dal.library(req.query.user_id, function(err, result){
            res.render('user/userLibrary', { vg: result[0]});
        });
    }

});

router.get('/library/videogame', function (req, res) {
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    if(req.query.vg_id == null) {
        res.send('vg_id is null');
    }
    else {
        user_dal.videogameInfo(req.query.user_id, req.query.vg_id, function(err, result){
            res.render('user/userVGInfo', { vg: result[0][0]});
        });
    }

});

router.get('/library/edit', function (req, res) {
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    if(req.query.vg_id == null) {
        res.send('vg_id is null');
    }
    else {
        user_dal.videogameInfo(req.query.user_id, req.query.vg_id, function(err, result){
            res.render('user/userUpdateVG', { vg: result[0][0]});
        });
    }

});

router.get('/library/update', function (req, res) {
    if(req.query.user_id == null) {
        res.send('user_id is null');
    }
    if(req.query.vg_id == null) {
        res.send('vg_id is null');
    }
    if(req.query.date_purchased == null) {
        res.send('Date Purchased is not defined');
    }
    else {
        user_dal.updateVG(req.query, function(err, result){
            res.redirect(302, '/user/all');
        });
    }

});

module.exports = router;
